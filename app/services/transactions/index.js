// Require module dependencies
const axios = require('axios')
const { pick, slice, sortBy } = require('lodash/fp')
const TransactionModel = require('../../db/models').Transaction

const transactionKeyFields = pick([
  'blockNumber',
  'timeStamp',
  'hash',
  'nonce',
  'blockHash',
  'transactionIndex',
  'from',
  'to',
  'value',
])

const upsertTransaction = (transaction) => {
  const query = transactionKeyFields(transaction)
  const update = transaction
  const options = { new: true, upsert: true, runValidators: true }

  return TransactionModel.findOneAndUpdate(
    query, update, options
  ).catch(
    console.error
  )
}

// TODO: evaluate https://github.com/sebs/etherscan-api
// TODO: move etherscan into a shared module
// NOTE: Returns up to a maximum of the last 10000 transactions only
//   https://etherscan.io/apis#accounts
const fetchByAddress = (address) => (
  axios.get(
    'https://api.etherscan.io/api',
    {
      params: {
        module: 'account',
        action: 'txlist',
        address,
        startblock: 0,
        endblock: 99999999,
        sort: 'asc',
        apikey: process.env.ETHERSCAN_API_KEY,
      },
    },
  ).then(
    // TODO: handle errors with retry & error logging
    (response) => response.data.result
  ).catch(
    console.error
  )
)

const storeTransactions = (transactions) => (
  transactions.map(upsertTransaction)
)

module.exports = {
  byAddress: ({ address, limit = Number.MAX_SAFE_INTEGER }) => (
    fetchByAddress(
      address
    ).then(
      storeTransactions
    ).then(
      (transactions) => Promise.all(transactions)
    ).then(
      sortBy('timeStamp')
    ).then(
      slice(0, limit)
    ).catch(
      console.error
    )
  ),
}
