// Require module dependencies
const axios = require('axios')
const bigInt = require('big-integer')
const { flow, map, reduce } = require('lodash/fp')
const { identity } = require('lodash')
const AddressModel = require('../../db/models').Address
const Transactions = require('../../services/transactions')

// TODO: move into a shared library
const bigIntAdd = (a, b) => bigInt(a).add(bigInt(b))

// TODO: this is needed if we want more than the last 10000 transactions
// const findTransactionsByAddress = (address) => {
//   const query = { $or: { from: address, to: address } }
//   const projection = ['from', 'to', 'value', 'gasUsed', 'gasPrice']
//   const options = {}
//
//   return TransactionModel.find(
//     query, update, options
//   ).catch(
//     console.error
//   )
// }

// updateBalance({ address: String, balance: `bigInt` | String }) => Address
const updateBalance = ({ address, balance }) => {
  const query = { address }
  const update = { balance: balance.toString() }
  const options = { new: true, upsert: true, runValidators: true }

  return AddressModel.findOneAndUpdate(
    query, update, options
  ).catch(
    console.error
  )
}

// TODO: evaluate https://github.com/sebs/etherscan-api
// TODO: move etherscan into a shared module
//
// Fetches the balance from Etherscan for the given address
//
// fetchBalance(String) => String (bigInt)
const fetchBalance = (address) => (
  axios.get(
    'https://api.etherscan.io/api',
    {
      params: {
        module: 'account',
        action: 'balance',
        address,
        tag: 'latest',
        apikey: process.env.ETHERSCAN_API_KEY,
      },
    },
  ).then(
    // TODO: handle errors with retry & error logging
    (response) => response.data.result
  ).catch(
    console.error
  )
)

// NOTE: A transaction should either have `to` or `contractAddress`
// NOTE: All integer values will be Strings and need converted to Big Int
// TODO: convert to Big Int in the database model
//
// Calculates debit or credit to the address based on transaction value and gas
// used
//
// totalTransactionAmount(String)(Transaction) => `bigInt`
const totalTransactionAmount = (address) => ({
  from,
  to,
  contractAddress,
  value,
  gasUsed,
  gasPrice,
}) => {
  const isSender = (address === from)
  const isRecipient = (address === to || address === contractAddress)

  if (!isSender && !isRecipient) { return bigInt(0) }

  const sign = bigInt(isRecipient ? 1 : -1)
  const gasCost = bigInt(gasUsed).multiply(bigInt(gasPrice))
  const totalAmount = isRecipient ? bigInt(value) : bigInt(value).add(gasCost)

  return totalAmount.multiply(sign)
}

// Given an address, returns a flow which operates on array of transactions to
// return a `bigInt` final balance of that wallet address
//
// balanceFromTransactions(String)([Transaction]) => `bigInt`
const balanceFromTransactions = (address) => (
  flow(
    map(totalTransactionAmount(address)),
    reduce(bigIntAdd, bigInt(0)),
  )
)

// Given an address, returns a flow which operates on array of transactions to
// return a `bigInt` final balance of that wallet address
//
// balanceFromTransactions(String)(`bigInt`|String) => Address
const storeBalance = (address) => (balance) => (
  updateBalance({ address, balance })
)

module.exports = {
  calculateBalance: (address) => (
    Transactions.byAddress({
      address
    }).then(
      balanceFromTransactions(address)
    ).then(
      storeBalance(address)
    ).catch(
      console.error
    )
  ),

  fetchBalance,
}
