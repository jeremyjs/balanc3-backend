// Require module dependencies
const graphqlHTTP = require('express-graphql')
const schema = require('./schema')
const resolver = require('./resolver')

module.exports = graphqlHTTP({
  schema: schema,
  rootValue: resolver,
  graphiql: true,
  formatError: error => ({
    message: error.message,
    locations: error.locations,
    stack: error.stack ? error.stack.split('\n') : [],
    path: error.path,
  })
})
