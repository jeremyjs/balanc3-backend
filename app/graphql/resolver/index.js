// Require module dependencies
const Addresses = require('../../services/addresses')
const Transactions = require('../../services/transactions')

// The resolver provides a function for each API endpoint
// WARNING: must use lowercase addresses
// TODO: figure out a better story for addresses
const resolver = {
  calculateBalance: ({ address }) => (
    Addresses.calculateBalance(
      address,
    ).catch(
      console.error
    )
  ),

  fetchBalance: ({ address }) => (
    Addresses.fetchBalance(
      address,
    ).then(
      (balance) => ({
        address,
        balance,
      })
    ).catch(
      console.error
    )
  ),

  transactions: ({ address, limit }) => (
    Transactions.byAddress({
       address,
       limit,
    }).catch(
      console.error
    )
  ),
}

module.exports = resolver
