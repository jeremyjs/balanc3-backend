// Require module dependencies
const { buildSchema } = require('graphql')

// Construct a schema, using GraphQL schema language
const schema = buildSchema(`
  type AddressMetadata {
    address: String
    balance: String
  }

  type Transaction {
    blockNumber: Int
    timeStamp: Int
    hash: String
    nonce: Int
    blockHash: String
    transactionIndex: Int
    from: String
    to: String
    isError: Int
    txreceipt_status: String
    input: String
    contractAddress: String

    # Big Integer Fields
    value: String
    gas: String
    gasPrice: String
    cumulativeGasUsed: String
    gasUsed: String
    confirmations: String
  }

  type Query {
    calculateBalance(address: String): AddressMetadata
    fetchBalance(address: String): AddressMetadata
    transactions(address: String, limit: Int): [Transaction]
  }
`)

module.exports = schema
