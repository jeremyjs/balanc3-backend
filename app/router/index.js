const express = require('express')
const routes = require('./routes')
const graphql = require('../graphql')

const router = express.Router()

/* GET home page. */
router.get('/', (req, res, next) => {
  res.redirect(routes.graphql)
})

router.use(routes.graphql, graphql)

module.exports = router
