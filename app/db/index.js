// Require module dependencies
const mongoose = require('mongoose')

// Define module variables
const prefix = '[Mongoose]'

// Connect to MongoDB instance
mongoose.connect('mongodb://localhost/balanc3-backend')

// Alias database connection
const db = mongoose.connection

// Configure event listeners
db.on('error', console.error.bind(console, `${prefix} Connection error:`))
db.once('open', () => console.info(`${prefix} Connection open.`))

// Export database connection
module.exports = db
