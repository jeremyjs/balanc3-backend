const mongoose = require('mongoose')
const Schema = mongoose.Schema

// TODO: convert to Big Int when document is returned from query
const Address = new Schema({
  // TODO: use a proper address validation library
  address: { type: String, lowercase: true, trim: true },
  isContractAddress: { type: Boolean },

  // Big Integer
  balance: { type: String },
})

module.exports = Address
