const mongoose = require('mongoose')
const Schema = mongoose.Schema

// TODO: convert to Big Int when document is returned from query
const Transaction = new Schema({
  blockNumber: { type: Number },
  timeStamp: { type: Number },
  hash: { type: String },
  nonce: { type: Number },
  blockHash: { type: String },
  transactionIndex: { type: Number },
  from: { type: String, lowercase: true, trim: true },
  to: { type: String, lowercase: true, trim: true },
  isError: { type: Number },
  txreceipt_status: { type: String },
  input: { type: String },
  contractAddress: { type: String, lowercase: true, trim: true },

  // Big Integer
  value: { type: String },
  gas: { type: String },
  gasPrice: { type: String },
  cumulativeGasUsed: { type: String },
  gasUsed: { type: String },
  confirmations: { type: String },
})

module.exports = Transaction
