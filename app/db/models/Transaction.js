const mongoose = require('mongoose')
const schemas = require('../schemas')
const TransactionSchema = schemas.Transaction

const Transaction = mongoose.model('Transaction', TransactionSchema)

module.exports = Transaction
