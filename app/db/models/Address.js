const mongoose = require('mongoose')
const schemas = require('../schemas')
const AddressSchema = schemas.Address

const Address = mongoose.model('Address', AddressSchema)

module.exports = Address
